/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.util;

import com.sun.lwuit.Component;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.list.ListCellRenderer;

/**
 *
 * @author hud
 */
public class AlternateColorRenderer extends Label implements ListCellRenderer {  
  
        private int[] colors = new int[]{0xFF0000, 0xFF00, 0xFF}; //Red , Green , Blue  
  
        private static int currentIndex;  
  
        public AlternateColorRenderer() {  
            super("");  
        }  
  
        public Component getListCellRendererComponent(List list, Object value, int index, boolean isSelected) {  
            setText(value.toString());  
            currentIndex = index % 3;  
  
            if (isSelected) {  
                setFocus(true);  
                getStyle().setBgTransparency(100);  
                getStyle().setFgColor(colors[currentIndex]);  
            } else {  
                setFocus(false);  
                getStyle().setBgTransparency(0);  
                getStyle().setFgColor(colors[currentIndex]);  
            }  
  
            return this;  
        }  
  
        public Component getListFocusComponent(List list) {  
            setText("");  
            setFocus(true);  
            getStyle().setBgTransparency(100);  
            return this;  
        }  
    }
