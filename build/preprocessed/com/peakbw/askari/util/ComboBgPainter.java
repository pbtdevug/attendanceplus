/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.util;

import com.sun.lwuit.Graphics;
import com.sun.lwuit.Painter;
import com.sun.lwuit.geom.Rectangle;

/**
 *
 * @author hud
 */
public class ComboBgPainter implements Painter
{
        private int bgcolor;

        public ComboBgPainter(int bgcolor)
        {
                this.bgcolor = bgcolor;
        }

        public void paint(Graphics g, Rectangle rect)
        {
                //probably redundant 
                //but save the current colour anyway
                int color = g.getColor();

                //set the given colour
                g.setColor(bgcolor);

                //get the position and dimension 
                //of rectangle to be drawn
                int x = rect.getX();
                int y = rect.getY();
                int wd = rect.getSize().getWidth();
                int ht = rect.getSize().getHeight();

                //draw the filled rectangle
                g.fillRect(x, y, wd, ht);

                //restore colour setting
                g.setColor(color);
        }
}
