/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.util;

import com.peakbw.askari.midlet.AttendanceMidlet;
import com.sun.lwuit.Dialog;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hud
 */
public class ConnectToServer implements Runnable {
    private AttendanceMidlet midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private static final String submit = "COMMIT";
    private HttpConnection conn = null;
    public String status,connectionURL;
    private String txNum,id="",shift,pin,location="",name="",confNum="",imei,
            currentDateTime,today,replyRootTag,upSignature,locationID,phoneNum;
    //private boolean exceptionOccured = false;
    
    public ConnectToServer(AttendanceMidlet midlet){
        this.midlet = midlet;
        //today = midlet.up.today;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        //blinker = th;
        status = null;
        th.start();
    }
    
    /*public void stop(){
        blinker = null;
    }*/
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String locationID,String trxnNum,String url,String imei){
        this.txNum = trxnNum;
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.locationID = locationID;
        this.location = location;
        this.connectionURL = url;
        this.imei =imei;
        //his.lctn = GuardManager.split(midlet.up.locationsHashTable.get(location).toString(),"#").elementAt(3).toString();
        //midlet.isBranchSupervisor = false;
    }
    
    public void run() {
        String url = connectionURL;
        byte [] bytes = null;
        int respCode = 0;
        replyRootTag = "reply";
        
        bytes = createTransactionXML(name,id,shift,pin,location,txNum,imei).getBytes();
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            os = conn.openOutputStream();
            os.write(bytes, 0, bytes.length);//write transaction to outputstream
            //os.flush();
            
            try{
                respCode = conn.getResponseCode();
            }
            catch(ConnectionNotFoundException ex){
                //save transaction under pend
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                midlet.activeTrxn.delete(0, midlet.activeTrxn.length());

                midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.staffNumber).append("*").append(txNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append("pending").append("*").append(today);
                //midlet.createSuccessForm(id,shift,name,midlet.number,txNum,midlet.up.today,location,status);
                
            }
            catch(IOException ex){
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                midlet.activeTrxn.delete(0, midlet.activeTrxn.length());

                midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.staffNumber).append("*").append(txNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append("pending").append("*").append(today);
                //midlet.createSuccessForm(id,shift,name,midlet.number,txNum,midlet.up.today,location,status);   
            }
            if (respCode == HttpConnection.HTTP_OK){
                midlet.connectionIsAvailable = true;
                is = conn.openInputStream();
                
                /*int ch ;
                StringBuffer sb = new StringBuffer();
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb.toString());*/

                viewXML(is);
                System.out.println(midlet.up.guardHashtable.toString());
                if(upSignature!= null){
                    RecordStore.deleteRecordStore("guardRecords");//remove previous records
                    byte [] byts = (upSignature+"*"+midlet.up.guardHashtable.toString()+"*"+midlet.up.locationsHashTable.toString()+"*"+midlet.up.itemsHashtable.toString()).getBytes();//get byte representation
                    RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
                    rst.addRecord(byts, 0, byts.length);
                    rst.closeRecordStore();//save update guard records
                }
                
                if(today==null||today.equals("null")){
                    today = midlet.up.today;
                }
                else{
                    //System.out.println(currentDateTime);
                    today = AttendanceMidlet.split(today, " ").elementAt(0).toString();
                }
                
                if(midlet.state == midlet.confirmationForm()&&status!=null){
                    if(status.equalsIgnoreCase("SUCCESSFUL")){
                        midlet.hisPendFlag = false;
                        midlet.connectionIsAvailable = true;
                        midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(phoneNum).append("*").append(confNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append(status).append("*").append(today);
                        midlet.saveTransaction(midlet.activeTrxn.toString());
                        midlet.dialog("", status, midlet.getTransition(),Dialog.TYPE_CONFIRMATION,midlet.successImage,5000);
                        //(date,shift,id,name,site,status)
                        midlet.showSuccessForm(today, shift, id, name,location, status);
                        //midlet.shiftForm().showBack();
                    }
                    else {
                        midlet.dialog("", status, midlet.getTransition(),Dialog.TYPE_ERROR,midlet.errorImage,5000);
                        midlet.confirmationForm().showBack();
                        midlet.connectionIsAvailable = true;
                    }
                }
                else{
                    //System.out.println(status);
                    midlet.connectionIsAvailable = true;
                }
            }
            else{
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                today = midlet.up.today;
                status = "";
                //exceptionOccured = true;
                midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                
                midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.staffNumber).append("*").append(txNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append("pending").append("*").append(today);
                //midlet.createSuccessForm(id,shift,name,midlet.number,txNum,midlet.up.today,location,status);
                
            }
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        catch (ConnectionNotFoundException ex) {
            //ex.printStackTrace();
            today = midlet.up.today;
            midlet.connectionIsAvailable = false;
            midlet.hisPendFlag = false;
            midlet.activeTrxn.delete(0, midlet.activeTrxn.length());

            midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.staffNumber).append("*").append(txNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append("pending").append("*").append(today);
            //midlet.createSuccessForm(id,shift,name,midlet.number,txNum,midlet.up.today,location,status);
            
        }
        catch (IOException ex) {
            //ex.printStackTrace();
            today = midlet.up.today;
            midlet.connectionIsAvailable = false;
            midlet.hisPendFlag = false;
            midlet.activeTrxn.delete(0, midlet.activeTrxn.length());

            midlet.activeTrxn.append("Attendance").append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.staffNumber).append("*").append(txNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append(status).append("*").append(today);
            // midlet.createSuccessForm(id,shift,name,midlet.number,txNum,midlet.up.today,location,status);
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally{
            if(is!=null){
                try {
                    is.close();
                } catch (IOException ex) {
                    
                }
            }
            if(os!=null){
                try {
                    os.close();
                } catch (IOException ex) {
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } catch (IOException ex) {
                }
            }
        }
    }
    
    public void viewXML(InputStream is){
        ParseEvent pe;
        String locaction = "";
        String action= new String();
        String idt = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),deploymentOfficerName = new String(),
                                deploymentOfficerID = new String(),deploymentOfficerMobileNumber= new String(),
                                officerDets,itemID = new String(),itemName = new String(),itemUnit = new String();
        
        try{
            InputStreamReader xmlReader = new InputStreamReader(is);
            XmlParser parser = new XmlParser( xmlReader );
            parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, replyRootTag);
            boolean trucking = true;
            while (trucking) {
                pe = parser.read();
                if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                    pe = parser.read();
                    txNum= pe.getText();
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                    pe = parser.read();
                    confNum = pe.getText();
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                    pe = parser.read();
                    status= pe.getText();
                    System.out.println("status = "+status);
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                    pe = parser.read();
                    currentDateTime= pe.getText();
                    today = currentDateTime;
                }
                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                    pe = parser.read();
                    upSignature = pe.getText();
                    System.out.println(upSignature);
                }
                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                    String nemu = pe.getName();
                    if (nemu.equals("update")) {
                        while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                            pe = parser.read();
                            if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                                String name1 = pe.getName();
                                if (name1.equals("record")) {
                                    while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                        pe = parser.read();
                                        if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                            pe = parser.read();
                                            gName = pe.getText();
                                            name = gName;
                                            System.out.println("update Name:"+name);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                            pe = parser.read();
                                            number = pe.getText();
                                            phoneNum = number;
                                            System.out.println("update num:"+phoneNum);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                            pe = parser.read();
                                            idt = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                            pe = parser.read();
                                            action = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                            pe = parser.read();
                                            locaction = pe.getText();
                                        }
                                    }
                                    //hashtable for guards
                                    if("INSERT".equalsIgnoreCase(action)){
                                        if(number==null||locaction==null){
                                            number = "null";
                                            locaction = "null";
                                        }
                                        midlet.up.guardHashtable.put(idt.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                    }
                                    else if("DELETE".equalsIgnoreCase(action)){
                                        midlet.up.guardHashtable.remove(idt.trim());
                                    }
                                }
                                else {
                                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                        pe = parser.read();
                                }
                            }
                            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("site")){
                                String nam = pe.getName();
                                if (nam.equals("deployment")) {
                                    while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                        pe = parser.read();
                                        if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteName")) {
                                            pe = parser.read();
                                            deploymentName = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                            pe = parser.read();
                                            action = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteID")) {
                                            pe = parser.read();
                                            deploymentID = pe.getText();
                                        }
                                    }
                                    //hashtable for locations
                                    officerDets = /*deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+*/deploymentName.trim();
                                    if("INSERT".equalsIgnoreCase(action)){
                                        midlet.up.locationsHashTable.put(deploymentID.trim(), officerDets);
                                    }
                                    else if("DELETE".equalsIgnoreCase(action)){
                                        midlet.up.locationsHashTable.remove(deploymentID.trim());
                                    }
                                }
                                else {
                                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                        pe = parser.read();
                                }
                            }
                        }
                    }
                    else {
                        while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                        pe = parser.read();
                    }
                }
                if (pe.getType() == Xml.END_TAG &&pe.getName().equals(replyRootTag))
                    trucking = false;
            }
        }
        catch(Exception ex){
        
        }
    }
    
    private String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum,String imei){
        //time = midlet.up.today;
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        
        if(name.equals("Guard")){
            name = "";
            xmlStr.append("<name>").append(name).append("</name>");
        }
        
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<siteID>").append(locationID).append("</siteID>");
        xmlStr.append("<siteName>").append(location).append("</siteName>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<submit>").append(ConnectToServer.submit).append("</submit>");
        xmlStr.append("<version>").append("").append("</version>");
        xmlStr.append("</record>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    } 
}
