/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.util;

import com.peakbw.askari.midlet.AttendanceMidlet;
import com.sun.lwuit.List;
import com.sun.lwuit.list.DefaultListModel;
import com.sun.lwuit.list.ListModel;
import java.util.Vector;
import javax.microedition.rms.RecordComparator;

/**
 *
 * @author hud
 */
public class FilterList extends List implements RecordComparator{
    private AttendanceMidlet att;
    
    public FilterList(Vector data,AttendanceMidlet am){
        //super();
        this.att = am;
        
        try{
            String [] items = new String[data.size()];
            for(int x=0;x<data.size();x++){
                items[x] = data.elementAt(x).toString(); 
            }
            
            String [] sortedResults = sortResults(items);
            
            for(int i=0;i<sortedResults.length;i++){
                String result = sortedResults[i];
                 //System.out.println("len = "+result.length());
                if(result.length()>19){
                    //System.out.println(">19");
                    this.addItem(result.substring(0,20));
                }
                else{
                    //System.out.println("<=19");
                    this.addItem(sortedResults[i]);
                }
            }
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
    }
    
    private String [] sortResults(String [] data) {
       // RecordSorter sorter = new RecordSorter();
        boolean changed = true;
        while (changed) {
            try{
            changed = false;
            for (int j = 0; j < (data.length - 1); j++) {
                String a = data[j], b = data[j + 1];
                if (a != null && b != null) {
                    int order = this.compare(a.getBytes(), b.getBytes());
                    if (order == FOLLOWS) {
                        changed = true;
                        data[j] = b;
                        data[j + 1] = a;
                    }
                }
            }
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        return data;
    }
    
    public Vector filter(String str,Vector data,ListModel model) {
        String temp,siteName;
        Vector tempData = new Vector();
        try{
            try{
                int siteID = Integer.parseInt(str);
                Object siteDetails = att.up.locationsHashTable.get(String.valueOf(siteID));
                if(siteDetails == null){
                    siteName = "";
                }
                else{
                    Vector locationsData = AttendanceMidlet.split(att.up.locationsHashTable.get(str).toString(),"#");
                    siteName = locationsData.elementAt(locationsData.size()-1).toString();
                }
            }
            catch(NumberFormatException ex){
                siteName = str;
            }
           removeAll(model);
            String [] items = new String[data.size()];
            for(int x=0;x<data.size();x++){
                items[x] = data.elementAt(x).toString(); 
            }
            String [] sortedResults = sortResults(items);
            int length = sortedResults.length;
            for (int j = 0; j < length; j++) {
                temp = (String) sortedResults[j];	
                if ((temp.toLowerCase()).startsWith(siteName.toLowerCase())) {
                    tempData.addElement(temp);
                    model.addItem(temp);
                }
            }
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
        return tempData;
    }

     public int compare(byte[] rec1, byte[] rec2) {
        int x = 0;
        try{
            String strRec1 = new String(rec1);
            String strRec2 = new String(rec2);
            x= strRec1.compareTo(strRec2) == 0 ? EQUIVALENT: (strRec1.compareTo(strRec2) > 0 ? FOLLOWS : PRECEDES);
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
        return x;
    }
     
    private void removeAll(ListModel list){
        int size = list.getSize();
        System.out.println("Size = "+size);
        for(int i=0;i<size;i++){
            list.removeItem(i);
        }
    }
}
