/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.midlet;

import com.peakbw.askari.util.ConnectToServer;
import com.peakbw.askari.util.FilterProxyListModel;
import com.peakbw.askari.util.Update;
import com.sun.lwuit.ComboBox;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Container;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Font;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.Slider;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.events.DataChangedListener;
import com.sun.lwuit.events.FocusListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.layouts.BoxLayout;
import com.sun.lwuit.list.DefaultListCellRenderer;
import com.sun.lwuit.list.DefaultListModel;
import com.sun.lwuit.plaf.UIManager;
import com.sun.lwuit.table.TableLayout;
import com.sun.lwuit.util.Resources;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.midlet.*;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

/**
 * @author hud
 */
public class AttendanceMidlet extends MIDlet implements FocusListener,DataChangedListener{
    
    public boolean connectionIsAvailable = false,historyFlag = false,pendingSendFlag =false,runingFirstTime = false,hisPendFlag = false;
    public StringBuffer activeTrxn = new StringBuffer();
    private ConnectToServer cts = new ConnectToServer(this);
    private RecordStore rst,historyRs;
    private int numRecords = 0;
    public String imei,connectionUrl,url,iccid,id,selectedSite,selectedSiteID,selectedShift,
            staffName,staffNumber;
    private String pin,selectedHistory,recordID;
    private boolean midletPaused = false;
    private Resources r;
    private int screenwidth,screenHeight;
    public Image alarmImage,errorImage,successImage,imgIcon,imgSplash;
    private Form splashScreen,pinForm,waitForm,configForm,mainMenu,supportForm,
            sitesForm,shiftForm,confirmationForm,historyForm,detailsForm;
    public Form state;
    private Label pbtLabel,iccidLabel,imeiLabel,urlLabel,dialogLabel,waitLabel,siteLabel,dateLabel,
            shiftLabel,idLabel,shiftLabel1,idLabel1,siteLabel1,dateLabel1,nameLabel,siteLabel2,
            nameLabel1,shiftLabel2,dateLabel2,idLabel2,statusLabel,multLineLabel;
    private TextField imeiTextField,iccidTextField,urlTextField,searchTextField,idTextField,siteTextField;
    private Transition trans;
    private Dialog dialog;
    private TableLayout tableLayout,tableLayout1,tableLayout2,tableLayout3,tableLayout4,tableLayout5,tableLayout6;
    private Command exitCommand,clearCommand,cancelCommand,historyCommand,submitCommand,newEntryCommand;
    private TextField pinTextArea;
    private TextArea supportTextArea;
    private Component activeComponent;
    private ComboBox shiftsMenu;
    private Slider slider;
    public Update up = new Update(this);
    private List siteList,historyList;
    private Hashtable locationANDid = new Hashtable();
    private DefaultListModel sitesListModel,historyListModel;
    private Vector recordIDs = new Vector();
    private Font systemFontMedium,systemFontSmall,systemFontLarge ;
    private FilterProxyListModel proxyModel;

    //<editor-fold defaultstate="collapsed" desc="AttendanceMidlet">
    public AttendanceMidlet(){
        String config;
        System.out.println("constractor inoked");
        
        try{
            rst = RecordStore.openRecordStore("config", true);
            numRecords = rst.getNumRecords();
            if(numRecords>0){
                RecordEnumeration re = rst.enumerateRecords(null, null, false);
                byte [] byts = null;
                while(re.hasNextElement()){
                    byts = re.nextRecord();
                    break;
                }
                config = new String(byts,0,byts.length);
                
                System.out.println("config String = "+config);
                
                Vector contents = split(config,"*");
                
                imei = contents.elementAt(0).toString();
                System.out.println(imei);
                iccid = contents.elementAt(1).toString();
                connectionUrl = contents.elementAt(2).toString();
                url = connectionUrl;
                System.out.println(url);
            }
            rst.closeRecordStore();
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="startApp">
    public void startApp() {
        System.out.println("App started");
        if(midletPaused){
            resumeMIDlet();
        }
        else{
            Display dis = Display.getInstance();
            Display.init(this);
        
            screenwidth = dis.getDisplayWidth();
            screenHeight = dis.getDisplayHeight();
        
            try {
                r = Resources.open("/res/resource.res");
                alarmImage = r.getImage("alarm");
                errorImage = r.getImage("error");
                successImage = r.getImage("success");
                imgIcon = r.getImage("imgIcon");
                
                systemFontMedium = r.getFont("systemMedium");
                systemFontLarge = r.getFont("systemLarge");
                systemFontSmall = r.getFont("systemSmall");
                
                UIManager.getInstance().setThemeProps(r.getTheme("appTheme"));
                getSplashScreen();
            } 
            catch (java.io.IOException e) {
                e.printStackTrace();
            }         
        }
        midletPaused = false;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="pauseApp">
    public void pauseApp() {
        midletPaused = true;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="destroyApp">
    public void destroyApp(boolean unconditional) {
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="resumeMIDlet">
    public void resumeMIDlet() {
        //pinForm().show();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" exitMIDlet">
    public void exitMIDlet() {
        destroyApp(true);
        notifyDestroyed();
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" split">
    public static Vector split(String splitStr, String delimiter) {
        //String [] splitArray;
            StringBuffer token = new StringBuffer();
            Vector tokens = new Vector();

            // split
            char[] chars = splitStr.toCharArray();
            for (int i=0; i < chars.length; i++) {
                if (delimiter.indexOf(chars[i]) != -1) {
                    // we bumbed into a delimiter
                    if (token.length() > 0) {
                        tokens.addElement(token.toString());
                        token.setLength(0);
                    }
                }
                else {
                    token.append(chars[i]);
                }
            }
            // don't forget the "tail"...
            if (token.length() > 0) {
                tokens.addElement(token.toString());
            }
            return tokens;
        }
    //</editor-fold> 
    
    //<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    private void  getSplashScreen() throws IOException {
        //log.debug("splashScreen");
        System.out.println("splash created");
        splashScreen = new Form();
        splashScreen.setLayout(new BorderLayout());
        splashScreen.setTitle("Askari Attendance");
        //splashScreen.addComponent(imageLabel());
        splashScreen.addComponent(BorderLayout.CENTER,pbtLabel());
        
        long startTime = System.currentTimeMillis();
        if(numRecords>0){
            splashScreen.show();
            int count = 0;
            //delay
            while(System.currentTimeMillis()-startTime <= 3000){
              count++;
              //System.out.println(count);
            }
            pinForm().show();
        }
        else{
            configForm().show();
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="pbtLabel">
    private Label pbtLabel(){
        if(pbtLabel == null){
            pbtLabel = new Label(imgSplash());
            pbtLabel.setAlignment(Component.CENTER);
            pbtLabel.getStyle().setBgTransparency(0);
            pbtLabel.setText("Powered by PBT");
            pbtLabel.setTextPosition(Component.BOTTOM);
            pbtLabel.getStyle().setFgColor(0xffffff);

        }
       return pbtLabel; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="imgSplash">
    private Image imgSplash(){
        if(imgSplash == null){
            imgSplash =  r.getImage("splashScreen");
        }
        return imgSplash;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="configForm">    
    private Form configForm(){
        if(configForm == null){ 
            configForm = new Form("Configurations");
            configForm.setSize(new Dimension(screenwidth,screenHeight));
            configForm.setScrollable(true);
            configForm.setScrollVisible(true);
            configForm.setLayout(tableLayout());
            TableLayout.Constraint constraint = tableLayout.createConstraint(1,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,imeiLabel());
            constraint = tableLayout.createConstraint(2,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,imeiTextField());
            constraint = tableLayout.createConstraint(3,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,iccidLabel());
            constraint = tableLayout.createConstraint(4,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,iccidTextField());
            constraint = tableLayout.createConstraint(5,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,urlLabel());
            constraint = tableLayout.createConstraint(6,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,urlTextField());
            configForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    imei = imeiTextField.getText();
                    System.out.println(imei);
                    iccid = iccidTextField.getText();
                    System.out.println(iccid);
                    url = urlTextField.getText();
                    System.out.println(url);
                    connectionUrl = url;
                    
                    imeiTextField.setText("");
                    iccidTextField.setText("");
                    urlTextField.setText("");
                    
                    if(imei.length()==15){
                        if(iccid.length()==19){
                            if(url.length()>20){
                                String config = imei+"*"+iccid+"*"+url;
                                byte[] bytes = config.getBytes();
                                try{
                                    RecordStore rstr = RecordStore.openRecordStore("config", true);
                                    rstr.addRecord(bytes, 0, bytes.length);
                                    rstr.closeRecordStore();
                                    
                                    System.out.println("config String = "+config);
                                    
                                     pinForm().show();
                                     
                                }
                                catch(RecordStoreException ex){
                                    ex.printStackTrace();
                                }
                                
                            }
                            else{
                                //Dialog.show("URL Error", "Invalid URL", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                                dialog("URL Error", "Invalid URL", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            }
                        }
                        else{
                           dialog("ICCID Error", "ICCID must be 19 digits", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000); 
                           //Dialog.show("ICCID Error", "ICCID must be 19 digits", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                        }
                    }
                    else{
                        dialog("IMEI Error", "IMEI must be 15 digits", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                        //Dialog.show("IMEI Error", "IMEI must be 15 digits", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                    }
                }
            });
        }
        return configForm;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout">    
    private TableLayout tableLayout(){
        if(tableLayout == null){ 
            tableLayout = new TableLayout(6,1);
            tableLayout.getPreferredSize(configForm);
        }
        return tableLayout;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout1">    
    private TableLayout tableLayout1(){
        if(tableLayout1 == null){ 
            tableLayout1 = new TableLayout(2,1);
            tableLayout1.getPreferredSize(sitesForm);
        }
        return tableLayout1;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout2">    
    private TableLayout tableLayout2(){
        if(tableLayout2 == null){ 
            tableLayout2 = new TableLayout(7,1);
            tableLayout2.getPreferredSize(shiftForm);
            
        }
        return tableLayout2;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout3">    
    private TableLayout tableLayout3(){
        if(tableLayout3 == null){ 
            tableLayout3 = new TableLayout(5,1);
            tableLayout3.getPreferredSize(confirmationForm);
        }
        return tableLayout3;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout4">    
    private TableLayout tableLayout4(){
        if(tableLayout4 == null){ 
            tableLayout4 = new TableLayout(6,1);
            tableLayout4.getPreferredSize(detailsForm);
        }
        return tableLayout4;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout5">    
    private TableLayout tableLayout5(){
        if(tableLayout5 == null){ 
            tableLayout5 = new TableLayout(1,1);
            tableLayout5.getPreferredSize(mainMenu);
        }
        return tableLayout5;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tableLayout6">    
    private TableLayout tableLayout6(){
        if(tableLayout6 == null){ 
            tableLayout6 = new TableLayout(1,1);
            tableLayout6.getPreferredSize(historyForm);
        }
        return tableLayout6;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="imeiTextField">    
    private TextField imeiTextField(){
        if(imeiTextField == null){
            imeiTextField = new TextField(TextField.NUMERIC);
            imeiTextField.setVisible(true);
            imeiTextField.setEditable(true);
            imeiTextField.setEnabled(true);
            imeiTextField.setHint("IMEI");
            imeiTextField.setInputMode("123");
            imeiTextField.setMaxSize(15);
            imeiTextField.setText("358779065777564");
            imeiTextField.setReplaceMenu(false);
            //imeiTextField.getStyle().setBgTransparency(200);
            imeiTextField.setColumns(6);
        }
        return imeiTextField;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="iccidTextField">    
    private TextField iccidTextField(){
        if(iccidTextField == null){
            iccidTextField = new TextField(TextField.NUMERIC);
            iccidTextField.setVisible(true);
            iccidTextField.setEditable(true);
            iccidTextField.setEnabled(true);
            iccidTextField.setHint("ICCID");
            iccidTextField.setInputMode("123");
            iccidTextField.setText("3543070508051198989");
            iccidTextField.setMaxSize(19);
            iccidTextField.setReplaceMenu(false);
            //iccidTextField.getStyle().setBgTransparency(200);
            iccidTextField.setColumns(6);
        }
        return iccidTextField;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="urlTextField">    
    private TextField urlTextField(){
        if(urlTextField == null){
            urlTextField = new TextField(TextField.NUMERIC);
            urlTextField.setText("http://askariplus.com/askari/backend/mobile/mobile.php");
            urlTextField.setVisible(true);
            urlTextField.setEditable(true);
            urlTextField.setEnabled(true);
            urlTextField.setHint("URL");
            urlTextField.setMaxSize(150);
            urlTextField.setInputMode("abc");
            urlTextField.setEnableInputScroll(true);
            //urlTextField.getStyle().setBgTransparency(200);
            urlTextField.setColumns(6);
        }
        return urlTextField;
    }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="searchTextField">    
    private TextField searchTextField(){
        if(searchTextField == null){
            searchTextField = new TextField(TextField.NUMERIC);
            searchTextField.setVisible(true);
            searchTextField.setEditable(true);
            searchTextField.setEnabled(true);
            searchTextField.setHint("Search");
            searchTextField.setMaxSize(6);
            searchTextField.setInputMode("abc");
            searchTextField.setEnableInputScroll(true);
            searchTextField.getStyle().setBgTransparency(200);
            searchTextField.setColumns(6);
            searchTextField.setUseSoftkeys(false);
            searchTextField.addFocusListener(this);
            searchTextField.addDataChangeListener(this);
        }
        return searchTextField;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="idTextField">    
    private TextField idTextField(){
        if(idTextField == null){
            idTextField = new TextField();
            idTextField.setVisible(true);
            idTextField.setEditable(true);
            idTextField.setEnabled(true);
            idTextField.setColumns(8);
            idTextField.setInputMode("123");
            //pinTextArea.setReplaceMenu(false);
            idTextField.setUseSoftkeys(false);
            idTextField.setMaxSize(8);
            //pinTextArea.setLabelForComponent(new Label("Enter Your Digit PIN"));
            idTextField.getStyle().setBgTransparency(200);
            idTextField.addFocusListener(this);
        }
        return idTextField;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="siteTextField">    
    private TextField siteTextField(){
        if(siteTextField == null){
            siteTextField = new TextField();
            siteTextField.setVisible(true);
            siteTextField.setEditable(true);
            siteTextField.setEnabled(true);
            siteTextField.setColumns(8);
            siteTextField.setInputMode("123");
            //pinTextArea.setReplaceMenu(false);
            siteTextField.setUseSoftkeys(false);
            siteTextField.setMaxSize(8);
            //pinTextArea.setLabelForComponent(new Label("Enter Your Digit PIN"));
            siteTextField.getStyle().setBgTransparency(200);
            siteTextField.addFocusListener(this);
        }
        return siteTextField;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="shiftLabel1">
    private Label shiftLabel1(){
        if(shiftLabel1 == null){
            shiftLabel1 = new Label("");
            shiftLabel1.getStyle().setFgColor(0xffffff);
            //shiftLabel1.getStyle().setFont(createSystemFont);
            shiftLabel1.getStyle().setBgTransparency(0);
        }
       return shiftLabel1; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="statusLabel">
    private Label statusLabel(){
        if(statusLabel == null){
            statusLabel = new Label("");
            //statusLabel.getStyle().setFont(createSystemFont);
            statusLabel.getStyle().setFgColor(0xffffff);
            statusLabel.getStyle().setBgTransparency(0);
        }
       return statusLabel; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="spaceLabel2">
    private Label idLabel2(){
        if(idLabel2 == null){
            idLabel2 = new Label("");
            //idLabel2.getStyle().setFont(createSystemFont);
            idLabel2.getStyle().setFgColor(0xffffff);
            idLabel2.getStyle().setBgTransparency(0);
        }
       return idLabel2; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="nameLabel1">
    private Label nameLabel1(){
        if(nameLabel1 == null){
            nameLabel1 = new Label("");
            //nameLabel1.getStyle().setFont(createSystemFont);
            nameLabel1.getStyle().setFgColor(0xffffff);
            nameLabel1.getStyle().setBgTransparency(0);
        }
       return nameLabel1; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="imeiLabel">
    private Label imeiLabel(){
        if(imeiLabel == null){
            imeiLabel = new Label("Enter Device IMEI");
            imeiLabel.getStyle().setFgColor(0xffffff);
            //imeiLabel.getStyle().setFont(createSystemFont);
            imeiLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return imeiLabel; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="iccidLabel">
    private Label iccidLabel(){
        if(iccidLabel == null){
            iccidLabel = new Label("Enter SIM ICCID");
            iccidLabel.getStyle().setFgColor(0xffffff);
            //iccidLabel.getStyle().setFont(createSystemFont);
            iccidLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return iccidLabel; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="urlLabel">
    private Label urlLabel(){
        if(urlLabel == null){
            urlLabel = new Label("Enter Connection URL");
            urlLabel.getStyle().setFgColor(0xffffff);
            //urlLabel.getStyle().setFont(createSystemFont);
            urlLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return urlLabel; 
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="siteLabel">
    private Label siteLabel(){
        if(siteLabel == null){
            siteLabel = new Label("");
            //siteLabel.getStyle().setFont(createSystemFont);
            siteLabel.getStyle().setFgColor(0xffffff);
            siteLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return siteLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="siteLabel2">
    private Label siteLabel2(){
        if(siteLabel2 == null){
            siteLabel2 = new Label("");
            //siteLabel2.getStyle().setFont(createSystemFont);
            siteLabel2.getStyle().setFgColor(0xffffff);
            siteLabel2.getStyle().setBgTransparency(0);
        }
       return siteLabel2;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="nameLabel">
    private Label nameLabel(){
        if(nameLabel == null){
            nameLabel = new Label("");
            //nameLabel.getStyle().setFont(createSystemFont);
            nameLabel.getStyle().setFgColor(0xffffff);
            nameLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return nameLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="siteLabel1">
    private Label siteLabel1(){
        if(siteLabel1 == null){
            siteLabel1 = new Label("");
            //siteLabel1.getStyle().setFont(createSystemFont);
            siteLabel1.getStyle().setFgColor(0xffffff);
            siteLabel1.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return siteLabel1;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dateLabel">
    private Label dateLabel(){
        if(dateLabel == null){
            dateLabel = new Label("");
            //dateLabel.getStyle().setFont(createSystemFont);
            dateLabel.getStyle().setFgColor(0xffffff);
            dateLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return dateLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dateLabel1">
    private Label dateLabel1(){
        if(dateLabel1 == null){
            dateLabel1 = new Label("");
            //dateLabel1.getStyle().setFont(createSystemFont);
            dateLabel1.getStyle().setFgColor(0xffffff);
            dateLabel1.getStyle().setBgTransparency(0);
        }
       return dateLabel1;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dateLabel2">
    private Label dateLabel2(){
        if(dateLabel2 == null){
            dateLabel2 = new Label("");
            //dateLabel2.getStyle().setFont(createSystemFont);
            dateLabel2.getStyle().setFgColor(0xffffff);
            dateLabel2.getStyle().setBgTransparency(0);
        }
       return dateLabel2;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="shiftLabel">
    private Label shiftLabel(){
        if(shiftLabel == null){
            shiftLabel = new Label("Select Shift");
            //shiftLabel.getStyle().setFont(createSystemFont);
            shiftLabel.getStyle().setFgColor(0xffffff);
            shiftLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return shiftLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="shiftLabel2">
    private Label shiftLabel2(){
        if(shiftLabel2 == null){
            shiftLabel2 = new Label("");
            //shiftLabel2.getStyle().setFont(createSystemFont);
            shiftLabel2.getStyle().setFgColor(0xffffff);
            shiftLabel2.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return shiftLabel2;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="idLabel">
    private Label idLabel(){
        if(idLabel == null){
            idLabel = new Label("Enter Staff ID");
            //idLabel.getStyle().setFont(createSystemFont);
            idLabel.getStyle().setFgColor(0xffffff);
            idLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return idLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="idLabel1">
    private Label idLabel1(){
        if(idLabel1 == null){
            idLabel1 = new Label("");
            //idLabel1.getStyle().setFont(createSystemFont);
            idLabel1.getStyle().setFgColor(0xffffff);
            idLabel1.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return idLabel1;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="shiftsMenu">
    private ComboBox shiftsMenu(){
        if(shiftsMenu == null){
            String [] options = {"DAY","NIGHT","ABSENT","SICK","LEAVE","OFF"};
            List list = new List(options);
            
            
            shiftsMenu = new ComboBox(list.getModel());
            
            DefaultListCellRenderer dlcr = (DefaultListCellRenderer)shiftsMenu.getRenderer();
            
            dlcr.getStyle().setFgColor(0xff0000);
            
            shiftsMenu.setVisible(true);
            shiftsMenu.setEnabled(true);
            shiftsMenu.setFocusable(true);
            shiftsMenu.getStyle().setBgTransparency(200);
            shiftsMenu.setFixedSelection(ComboBox.FIXED_NONE);
            shiftsMenu.addFocusListener(this);
            shiftsMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    ComboBox list = (ComboBox)ae.getComponent();
                    selectedShift = list.getSelectedItem().toString();
                    System.out.println("Shift = "+selectedShift);
                }
            });
        }
        return shiftsMenu;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getTransition">
    public Transition getTransition(){
        if(trans == null){
            trans = CommonTransitions.createFade(1000);
        }
        return trans;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dialog">  
    //show("Error", new Label(status), null, Dialog.TYPE_INFO, null, 5000,getTransition());
    public void dialog(String title,String msg,Transition trans,int type,Image icon,int timeOut){
        //System.out.println("Dialog");
        //state.show();
        //if(dialog == null){ 
            //System.out.println("Dialog1");
            dialog = new Dialog();
            //dialog.getStyle().setBgTransparency(100);
            dialog.getStyle().setBgColor(timeOut);
            dialog.getStyle().setBackgroundGradientEndColor(0xfb060e);
            dialog.setLayout(new BorderLayout());
            dialog.setScrollable(true);
            //dialog.setTitle(msg);
            dialog.addComponent(BorderLayout.CENTER,multLineLabel(msg));
            dialog.addComponent(BorderLayout.NORTH,dialogLabel(icon));
            //System.out.println("Dialog5");
            dialog.flushReplace();
            //System.out.println("Dialog6");
            dialog.setDialogType(type);
            dialog.setTransitionOutAnimator(trans);
            dialog.setTransitionInAnimator(trans);
            dialog.setTimeout(timeOut);
            //System.out.println("Dialog7");
        //}
        //dialog.show();    
        dialog.showPacked("Center", true);
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dialogLabel">
    private Label dialogLabel(Image img){
        //if(dialogLabel == null){
            dialogLabel = new Label(img);
            //imageLabel.getStyle().setFgColor(0xffffff);
            //dialogLabel.getStyle().setBorder(Border.createLineBorder(2));
            dialogLabel.setAlignment(Component.CENTER);
        //}
       return dialogLabel; 
    }
    
//</editor-fold> 
        
    //<editor-fold defaultstate="collapsed" desc="exitCommand">
    private Command exitCommand() {
        if (exitCommand == null) {
            exitCommand = new Command("Exit", 1);
        }
        return exitCommand;
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="clearCommand">
    private Command clearCommand() {
        if (clearCommand == null) {
           clearCommand = new Command("Clear", 2);
        }
        return clearCommand;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="cancelCommand ">
    private Command cancelCommand() {
        if (cancelCommand == null) {
            cancelCommand = new Command("Cancel", 0);
        }
        return cancelCommand;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="submitCommand ">
    private Command submitCommand() {
        if (submitCommand == null) {
            submitCommand = new Command("Submit", 0);
        }
        return submitCommand;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="newEntryCommand">
    private Command newEntryCommand() {
        if (newEntryCommand == null) {
            newEntryCommand = new Command("New Enrty", 0);
        }
        return newEntryCommand;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="historyCommand">
    private Command historyCommand() {
        if (historyCommand == null) {
            historyCommand = new Command("History", 1);
        }
        return historyCommand;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="pinForm">    
    public Form pinForm(){
        if(pinForm == null){
            pinForm = new Form();
            pinForm.setTitle("Enter Your 4 Digit PIN");
            pinForm.setLayout(new BorderLayout());
            pinForm.addComponent(BorderLayout.CENTER,pinTextArea());
            //pinForm.addCommand(okCommand());
            pinForm.addCommand(exitCommand());
            pinForm.addCommand(clearCommand());
            pinForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    //if(evt.getSource() == Canvas.FIRE){
                        pin = pinTextArea.getText();
                        pinTextArea.setText("");
                        if(pin.length()!=4){
                            //log.debug("PIN length != 4");
                            //Dialog.show("Exception", e.getMessage(), "OK", null);
                            //Dialog.setAutoAdjustDialogSize(true);
                            //Dialog.
                            dialog("PIN Error", "PIN must be 4 digits", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            //Dialog.show("PIN Error", new Label("PIN must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                        }
                        else{
                            waitForm().show();
                            up.startUpdate(pin,imei,url);
                        }
                    //}
                }
            });
            pinForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == clearCommand){
                        String content = pinTextArea.getText();
                        int length = content.length();
                        if(length>0){
                            content = content.substring(0, length-1);
                            pinTextArea.setText(content);
                        }
                    }
                    else{
                        //log.debug("Exit Command Selected");
                        exitMIDlet();
                    }
                }
            });
        }
        System.out.println("pinForm showing");
        return pinForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="supportForm">    
    public Form supportForm(){
        if(supportForm == null){
            supportForm = new Form();
            supportForm.setTitle("Technical Support");
            supportForm.setLayout(new BorderLayout());
            supportForm.addComponent(BorderLayout.CENTER,supportTextArea());
            supportForm.addCommand(cancelCommand());
            supportForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == cancelCommand){
                        mainMenu().showBack();
                    }
                }
            });
        }
        return supportForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="sitesForm">    
    public Form sitesForm(){
        if(sitesForm == null){
            sitesForm = new Form();
            sitesForm.setTitle("Select Site");
            sitesForm.setSize(new Dimension(screenwidth,screenHeight));
            sitesForm.setScrollable(true);
            sitesForm.setScrollVisible(true);
            sitesForm.setLayout(tableLayout1());
            TableLayout.Constraint constraint = tableLayout1.createConstraint(1,0);
            constraint.setWidthPercentage(100);
            sitesForm.addComponent(constraint,searchTextField());
            constraint = tableLayout1.createConstraint(2,0);
            constraint.setWidthPercentage(100);
            sitesForm.addComponent(constraint,siteList());
            sitesForm.addCommand(cancelCommand());
            /*sitesForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    searchTextField.setText("");
                    selectedSite = sitesListModel.getItemAt(newListSelectedIndex).toString();
                    System.out.println("Selected Site = "+selectedSite);
                    selectedSiteID = locationANDid.get(selectedSite).toString();
                    idTextField().setText("");
                    siteLabel().setText("Site: "+selectedSite);
                    dateLabel().setText(getDateTime());
                    shiftForm().show();
                }
            });*/
            sitesForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    Command cmd = evt.getCommand();
                    if(cmd == cancelCommand){
                        mainMenu().showBack();
                    }
                    else if(cmd == clearCommand){
                        TextField textField = ((TextField)activeComponent);
                        String content = textField.getText();
                        int length = content.length();
                        if(length>0){
                            content = content.substring(0, length-1);
                            textField.setText(content);
                        }
                    }
                    else if(cmd == historyCommand){
                        state = sitesForm;
                        historyForm().show();
                        getHistory();
                    }
                }
            });
        }
        
        return sitesForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="historyForm">    
    public Form historyForm(){
        if(historyForm == null){
            historyForm = new Form();
            historyForm.setTitle("History");
            historyForm.setSize(new Dimension(screenwidth,screenHeight));
            historyForm.setScrollable(true);
            historyForm.setScrollVisible(true);
            historyForm.setLayout(tableLayout6());
            TableLayout.Constraint constraint = tableLayout6.createConstraint(1,0);
            constraint.setWidthPercentage(100);
            historyForm.addComponent(constraint,historyList());
            historyForm.addCommand(cancelCommand());
            historyForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == cancelCommand){
                        sitesForm().showBack();
                    }
                }
            });
        }
        
        return historyForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="mainMenu">    
    public Form mainMenu(){
        if(mainMenu == null){
            List list = new List();
            list.addItem("Attendance");
            list.addItem("Technical Support");
            DefaultListCellRenderer dlcr = (DefaultListCellRenderer)list.getRenderer();
            dlcr.getStyle().setFgColor(0xFFFFFF);
            list.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    String selectedItem = ((List)ae.getComponent()).getSelectedItem().toString();
                    if(selectedItem.equals("Technical Support")){
                        supportForm().show();
                    }
                    else{
                        state = mainMenu;
                        //sitesForm().show();
                        idTextField().setText("");
                        siteLabel().setText("Enter Site ID");
                        
                        dateLabel().setText(getDateTime());
                        shiftForm().show();
                    }
                }
            });
            mainMenu = new Form();
            mainMenu.setTitle("Askari Attendance");
            mainMenu.setLayout(tableLayout5());
            TableLayout.Constraint constraint = tableLayout5.createConstraint(1,0);
            constraint.setWidthPercentage(100);
            mainMenu.addComponent(constraint,list);
            mainMenu.addCommand(exitCommand());
            mainMenu.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == exitCommand){
                       exitMIDlet();
                    }
                }
            });
        }
        return mainMenu;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="shiftForm">    
    public Form shiftForm(){
        if(shiftForm == null){
            shiftForm = new Form();
            shiftForm.setTitle("Attendance");
            shiftForm.setSize(new Dimension(screenwidth,screenHeight));
            shiftForm.setScrollable(true);
            shiftForm.setScrollableY(true);
            shiftForm.setScrollVisible(true);
            //shiftForm.setLayout(tableLayout2());
            
            /*TableLayout.Constraint constraint = tableLayout2.createConstraint(1,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(20);
            shiftForm.addComponent(constraint,dateLabel());*/
            
            /*constraint = tableLayout2.createConstraint(2,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            shiftForm.addComponent(constraint,spaceLabel());*
            
            /*constraint = tableLayout2.createConstraint(4,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            shiftForm.addComponent(constraint,spaceLabel1());*/
            
            /*constraint = tableLayout2.createConstraint(2,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(10);
            shiftForm.addComponent(constraint,shiftLabel());
            
            constraint = tableLayout2.createConstraint(3,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setWidthPercentage(100);
            shiftForm.addComponent(constraint,shiftsMenu());
            
            /*constraint = tableLayout2.createConstraint(7,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            shiftForm.addComponent(constraint,spaceLabel2());*/
            
            /*constraint = tableLayout2.createConstraint(4,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(20);
            shiftForm.addComponent(constraint,siteLabel());
            
            constraint = tableLayout2.createConstraint(5,0);
            constraint.setWidthPercentage(100);
            shiftForm.addComponent(constraint,siteTextField());
            
            constraint = tableLayout2.createConstraint(6,0);
            constraint.setWidthPercentage(100);
            constraint.setHeightPercentage(18);
            shiftForm.addComponent(constraint,idLabel());
            
            constraint = tableLayout2.createConstraint(7,0);
            constraint.setWidthPercentage(100);
            shiftForm.addComponent(constraint,idTextField());*/
            
            Container container = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            container.addComponent(dateLabel());
            container.addComponent(shiftLabel());
            container.addComponent(shiftsMenu());
            container.addComponent(siteLabel());
            container.addComponent(siteTextField());
            container.addComponent(idLabel());
            container.addComponent(idTextField());
            
            shiftForm.setLayout(new BorderLayout()); 
            shiftForm.addComponent(BorderLayout.CENTER, container);
            
            shiftForm.addCommand(cancelCommand());
            shiftForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    id = idTextField.getText();
                    
                    selectedSiteID = siteTextField.getText();
                    
                    selectedShift = shiftsMenu.getSelectedItem().toString();
                    
                    if(activeComponent.equals(idTextField) || activeComponent.equals(siteTextField)){        
                        System.out.println("StaffID = "+id);
                        if(id.length()>=1){
                            
                            if(!(up.locationsHashTable.get(selectedSiteID)==null)){
                                selectedSite = up.locationsHashTable.get(selectedSiteID).toString();
                                
                                System.out.println("SiteName = "+selectedSite);
                                
                                dateLabel1().setText(getDateTime());
                                idLabel1().setText("StaffID: "+id);
                                shiftLabel1().setText("Shift: "+selectedShift);
                                siteLabel1().setText("Site: "+selectedSite);
                                if(!(up.guardHashtable.get(id)==null)){
                                    Vector guardDetails = split(up.guardHashtable.get(id).toString(),"_");//split returned string by _
                                    staffNumber = guardDetails.elementAt(1).toString();
                                    staffName = guardDetails.elementAt(0).toString();
                                    nameLabel().setText("Name: "+staffName);
                                    state = shiftForm;
                                    confirmationForm().show();
                                }
                                else{
                                    staffName = "Guard";
                                    nameLabel().setText("Name: "+staffName);
                                    confirmationForm().show();
                                    dialog("Warnning", "Staff is not in the phone database", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                }
                            }else{
                                dialog("Site Error", "Unknown Site ID", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            }
                        }
                        else{
                            dialog("ID Error", "Staff ID must be atleast \n1 character", getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                        }
                    }
                }
            });
            shiftForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == cancelCommand){
                        mainMenu.showBack();
                    }
                    else if(evt.getCommand() == clearCommand){
                        TextField textField = ((TextField)activeComponent);
                        String content = textField.getText();
                        int length = content.length();
                        if(length>0){
                            content = content.substring(0, length-1);
                            textField.setText(content);
                        }
                    }
                }
            });
        }
        return shiftForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="confirmationForm">    
    public Form confirmationForm(){
        if(confirmationForm == null){
            confirmationForm = new Form();
            confirmationForm.setTitle("Confirm Details");
            confirmationForm.setSize(new Dimension(screenwidth,screenHeight));
            confirmationForm.setScrollable(true);
            confirmationForm.setScrollVisible(true);
            confirmationForm.setLayout(tableLayout3());
            TableLayout.Constraint constraint = tableLayout3.createConstraint(1,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            confirmationForm.addComponent(constraint,dateLabel1());
            constraint = tableLayout3.createConstraint(2,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            confirmationForm.addComponent(constraint,idLabel1());
            constraint = tableLayout3.createConstraint(3,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            confirmationForm.addComponent(constraint,shiftLabel1());
            constraint = tableLayout3.createConstraint(4,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            confirmationForm.addComponent(constraint,nameLabel());
            constraint = tableLayout3.createConstraint(5,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            confirmationForm.addComponent(constraint,siteLabel1());
            confirmationForm.addCommand(cancelCommand());
            confirmationForm.addCommand(submitCommand());
            confirmationForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    Command cmd = evt.getCommand();
                    if(cmd == cancelCommand){
                        shiftForm().showBack();
                    }
                    else if(cmd == submitCommand){
                        siteTextField.setText("");
                        idTextField.setText("");
                        state = confirmationForm;
                        cts.setTransaction(staffName, id, selectedShift, pin, selectedSite,selectedSiteID, getTrxnNumber(),url,imei);
                        cts.startConnection();
                        waitForm().setTitle("Checking against schedule.....");
                        waitForm().showBack();
                    }
                }
            });
            confirmationForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    state = confirmationForm;
                    siteTextField.setText("");
                    idTextField.setText("");
                    
                    cts.setTransaction(staffName, id, selectedShift, pin, selectedSite,selectedSiteID, getTrxnNumber(),url,imei);
                    cts.startConnection();
                    waitForm().setTitle("Checking against schedule.....");
                    waitForm().showBack();
                }
            });
        }
        return confirmationForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="detailsForm">    
    public Form detailsForm(){
        if(detailsForm == null){
            detailsForm = new Form();
            detailsForm.setTitle("History");
            detailsForm.setSize(new Dimension(screenwidth,screenHeight));
            detailsForm.setScrollable(true);
            detailsForm.setScrollVisible(true);
            detailsForm.setLayout(tableLayout4());
            TableLayout.Constraint constraint = tableLayout4.createConstraint(1,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,dateLabel2());
            constraint = tableLayout4.createConstraint(2,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,idLabel2());
            constraint = tableLayout4.createConstraint(3,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,shiftLabel2());
            constraint = tableLayout4.createConstraint(4,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,nameLabel1());
            constraint = tableLayout4.createConstraint(5,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,siteLabel2());
            constraint = tableLayout4.createConstraint(6,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(18);
            detailsForm.addComponent(constraint,statusLabel());
            detailsForm.addCommand(cancelCommand());
            detailsForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    Command cmd = evt.getCommand();
                    if(cmd == cancelCommand){
                        if(state == confirmationForm){
                            mainMenu().showBack();
                        }
                        else{
                            historyForm().showBack();
                        }
                        getHistory();
                    }
                    else if(cmd == newEntryCommand){
                        detailsForm.removeCommand(newEntryCommand());
                        shiftForm().showBack();
                    }
                }
            });
        }
        return detailsForm;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="siteList"> 
    private List siteList(){
        if(siteList == null){
            Vector sites = getSites();
            sitesListModel = new DefaultListModel(sites);
            proxyModel = new FilterProxyListModel(sitesListModel);
            siteList = new List( proxyModel);
            
            DefaultListCellRenderer dlcr = (DefaultListCellRenderer)siteList.getRenderer();
            dlcr.getStyle().setFgColor(0xffffff);
            
            siteList.setSmoothScrolling(true);  
            siteList.getStyle().setBgTransparency(0);
            siteList.setFixedSelection(List.FIXED_NONE);
            siteList.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    searchTextField.setText("");
                    selectedSite = siteList.getSelectedItem().toString();
                    System.out.println("Selected Site = "+selectedSite);
                    selectedSiteID = locationANDid.get(selectedSite).toString();
                    idTextField().setText("");
                    siteLabel().setText("Site: "+selectedSite);
                    dateLabel().setText(getDateTime());
                    shiftForm().show();
                }
            });
        }
        return siteList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="historyList"> 
    private List historyList(){
        if(historyList == null){
            
            historyListModel = new DefaultListModel();
            
            historyList = new List(historyListModel);
            historyList.getStyle().setFont(systemFontSmall);
            DefaultListCellRenderer dlcr = (DefaultListCellRenderer)historyList.getRenderer();
            dlcr.getStyle().setFgColor(0xFFFFFF);
            dlcr.getStyle().setFont(systemFontSmall);
            historyList.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    
                    selectedHistory = ((List)ae.getComponent()).getSelectedItem().toString();
                    System.out.println("Selected History = "+selectedHistory);
                    
                    recordID = (recordIDs.elementAt(historyList.getSelectedIndex())).toString();
                    
                    decodeTransaction(recordID,"history");//converts transaction back to string format
                }
            });
        }
        return historyList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="pinTextArea">    
    private TextField pinTextArea(){
        if(pinTextArea == null){
            pinTextArea = new TextField();
            pinTextArea.setVisible(true);
            pinTextArea.setEditable(true);
            pinTextArea.setEnabled(true);
            pinTextArea.setColumns(20);
            pinTextArea.setInputMode("123");
            //pinTextArea.setReplaceMenu(false);
            pinTextArea.setUseSoftkeys(false);
            pinTextArea.setMaxSize(4);
            //pinTextArea.setLabelForComponent(new Label("Enter Your Digit PIN"));
            //pinTextArea.getStyle().setBgTransparency(200);
            pinTextArea.addFocusListener(this);
        }
        return pinTextArea;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="supportTextArea">    
    private TextArea supportTextArea(){
        if(supportTextArea == null){
            supportTextArea = new TextArea();
            supportTextArea.setVisible(true);
            supportTextArea.setEditable(false);
            supportTextArea.setEnabled(false);
            supportTextArea.setText("Call us on:\n\n0781191914\n0783131266");
            supportTextArea.setColumns(5);
            //supportTextArea.getStyle().setBgTransparency(200); //
        }
        return supportTextArea;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="multLineLabel">    
    private Label multLineLabel(String msg){
        //if(multLineLabel == null){
        multLineLabel = new Label(msg);
        multLineLabel.setVisible(true);
        multLineLabel.setEnabled(false);
        multLineLabel.getStyle().setFont(systemFontSmall);
        //}
        return multLineLabel;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="focusGained">
    public void focusGained(Component cmpnt) {
        activeComponent = cmpnt;
        if(activeComponent.equals(idTextField) || activeComponent.equals(siteTextField)){
            shiftForm().addCommand(clearCommand());
        }
        else if(activeComponent.equals(searchTextField)){
            sitesForm().addCommand(clearCommand());
            sitesForm().removeCommand(historyCommand());
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="focusLost">
    public void focusLost(Component cmpnt) {
        activeComponent = cmpnt;
        //if(((TextField)activeComponent).equals(idTextField)){
            shiftForm().removeCommand(clearCommand());
        //}
        //else if(((TextField)activeComponent).equals(searchTextField)){
            sitesForm().removeCommand(clearCommand());
            sitesForm().addCommand(historyCommand());
        //}
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getDateTime">    
    public static String getDateTime() {//Fri 29 Nov 2013
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(5).toString()+"-"+mnt+"-"+dateInfo.elementAt(2);
        return txn;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="waitForm">    
    private Form waitForm(){
        if(waitForm == null){ 
            waitForm = new Form("Checking for updates");
            waitForm.setLayout(new BorderLayout());
            waitForm.addComponent(BorderLayout.SOUTH,slider());
            waitForm.addComponent(BorderLayout.CENTER,waitLabel());
            waitForm.addCommand(cancelCommand());
            waitForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(state!=null){
                        pinForm().show();
                    }
                    else{
                        pinForm().show();
                    }
                }
            });
        }
        System.out.println("waitForm showing");
        return waitForm;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="slider">    
    private Slider slider(){
        if(slider == null){ 
            slider = new Slider();
            slider.setInfinite(true);
            slider.animate();
            slider.getStyle().setBgTransparency(200);
            slider.setText("please wait");
            slider.setSmoothScrolling(true);
            slider.getStyle().setFgColor(0xffffff);
        }
        return slider;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="waitLabel">
    private Label waitLabel(){
        if(waitLabel == null){
            waitLabel = new Label("Connecting ....");
            //otpLabel.getStyle().set3DText(true, true);
            waitLabel.getStyle().setBgTransparency(0);
            waitLabel.getStyle().setFgColor(0xffffff);
        }
       return waitLabel; 
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getSites">
    private Vector getSites(){
        Vector locations = new Vector();
        try{
            System.out.println(up.locationsHashTable.toString());
            Enumeration locationsEnum = up.locationsHashTable.keys();
            String locationName,locationID;
            while(locationsEnum.hasMoreElements()){
                locationID = locationsEnum.nextElement().toString();
                Vector locationsData = split(up.locationsHashTable.get(locationID).toString(),"#");
                locationName = locationsData.elementAt(locationsData.size()-1).toString();
                locations.addElement(locationName);
                locationANDid.put(locationName, locationID);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return locations;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dataChanged">
    public void dataChanged(int type, int index) {
        String searchContent = searchTextField.getText();
        //siteList.filter(searchContent, getSites(), siteList.getModel());
        proxyModel.filter(searchContent);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" getTrxnNumber">    
    public String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getHistory">
    private void getHistory(){
        RecordEnumeration re;
        //Vector vector = new Vector();
        historyListModel.removeAll();
        try{
            historyRs = RecordStore.openRecordStore("history", true);
            if(historyRs.getNumRecords()>0){
                historyFlag = true;//flag distinguishes history from pending transactions
                re = historyRs.enumerateRecords(null, null, false);// all history transactions
                //recordIDs is a vector that stores all ids for histroy transactions as they occur in the record store 
                recordIDs.removeAllElements();//clear previous ids
                //int count = 1;
                while(re.hasNextElement()){//access each transaction one at a time
                    byte []bytes = re.nextRecord();
                    String record = new String(bytes,0,bytes.length);//represents a single history transaction
                    Vector det = split(split(record,"?").elementAt(1).toString(),"*");//history transaction is a string concatinated by * therefore spliting by that
                    //sample record: 1?Attendance*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*2014-01-31
                    //2?Schedule Shifts*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
                    //Supervisor Tour*4545*343434*INCIDENT*broken glass*200214200120*9991*pending*2014-01-09
                    //Store Manager*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
                    String action = det.elementAt(0).toString();
                    //System.out.println(flg);
                    historyListModel.addItem(det.elementAt(10)+"->ID:"+det.elementAt(2));
                    //vector.addElement(det.elementAt(10)+"->ID:"+det.elementAt(2));
                    //historyPendingChoiceGroup().setFont(, getFont());
                    recordIDs.addElement(split(record,"?").elementAt(0).toString());//add id eg 1
                    //System.out.println(recordIDs);
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        //return vector;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" saveTransaction"> 
    public void saveTransaction(String activeTrxin){
        //activeTrxn: MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
        try{
            RecordStore rstr;
            if(connectionIsAvailable==false){
                rstr = RecordStore.openRecordStore("pending", true);
            }
            else{
                rstr = RecordStore.openRecordStore("history", true);
            }
            int idt = rstr.getNextRecordID();
            String transaction = idt+"?"+activeTrxin;//16?MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013                                                                              
            //0*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*2014-01-31
            byte [] bytes1 = transaction.getBytes();
            rstr.addRecord(bytes1, 0, bytes1.length);
            rstr.closeRecordStore();
            activeTrxn.delete(0, activeTrxn.length());
            System.out.println(transaction);
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" decodeTransaction">
    public void decodeTransaction(String recordID,String recordStoreName){ // creates back transaction saved as history from RecordSore object to string object
        try{
            RecordStore rs = RecordStore.openRecordStore(recordStoreName, true);
            byte []bytes = rs.getRecord(Integer.parseInt(recordID));
            String record = new String(bytes,0,bytes.length);
            //System.out.println(record);
            Vector trxn = split(split(record,"?").elementAt(1).toString(),"*");//16?MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
            //0*BOGERE HUDSON*9991*DAY*783131266*1391331348*20003*STANBIC/Jinja_Main_Office*9991*2014-02-02
            //1*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
            //2*OKETCH LIVINGSTONE*12345*9000*INCIDENT*come now*300114104024*9991*2014-01-09
            //String action = trxn.elementAt(0).toString();
            //createSuccessForm(guardID,shift,name,number,confNumber,date,location)
            dateLabel2().setText("Shift Date: "+trxn.elementAt(10).toString());
            shiftLabel2().setText("Shift Type: "+trxn.elementAt(3).toString());
            idLabel2().setText("Staff ID: "+trxn.elementAt(2).toString());
            nameLabel1().setText("Name: "+trxn.elementAt(1).toString());
            siteLabel2().setText("Site: "+trxn.elementAt(7).toString());
            statusLabel().setText("Status: "+trxn.elementAt(9).toString());
            //createSuccessForm(trxn.elementAt(2).toString(),trxn.elementAt(3).toString(),trxn.elementAt(1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(5).toString(),trxn.elementAt(10).toString(),trxn.elementAt(7).toString(),trxn.elementAt(9).toString());
            rs.closeRecordStore();
            detailsForm().show();
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="showSuccessForm">
    public void showSuccessForm(String date,String shift,String id,String name,String site,String status){
        dateLabel2().setText("Shift Date: "+date);
        shiftLabel2().setText("Shift Type: "+shift);
        idLabel2().setText("Staff ID: "+id);
        nameLabel1().setText("Name: "+name);
        siteLabel2().setText("Site: "+site);
        statusLabel().setText("Status: "+status);
        
        detailsForm().addCommand(newEntryCommand());
        detailsForm().show();
    }
    //</editor-fold>
}


