/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peakbw.askari.util;

import com.peakbw.askari.midlet.AttendanceMidlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hud
 */
public class Update implements Runnable {
    private AttendanceMidlet midlet;
    private OutputStream os = null;
    private InputStream is = null,fis = null;
    private HttpConnection conn = null;
    private String currentDateTime;
    public String today = AttendanceMidlet.getDateTime(),updateSign,pin,upSignature,imei,connectionURL;
    public Vector codes,unitsVector = new Vector();
    public Hashtable guardHashtable = new Hashtable(),locationsHashTable = new Hashtable(),itemsHashtable = new Hashtable();
    public boolean firstUpdate = true;
    public static String upgradeURL;
    
    public Update(AttendanceMidlet mid){
        this.midlet = mid;
    }
    
    public void startUpdate(String pin,String imei,String url){
        this.pin = pin;
        this.updateSign = decodeGuardRecords();
        this.imei = imei;
        this.connectionURL = url;
        
        upSignature = null;
        
        Thread t = new Thread(this);
        t.start();//starts run() or new thread
        
        //updateTaskSheduler();
        
    }
    
    private String createUpdateXML(String imei,String upSign,String pin){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<updateReq>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<signature>").append(upSign).append("</signature>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("</updateReq>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    public void run() {
        String url = connectionURL;
        System.out.println(url);
        int respCode = 0;
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            //open stream connection
            os = conn.openOutputStream();
            byte [] bytes = createUpdateXML(imei,updateSign,pin).getBytes();
            os.write(bytes, 0, bytes.length);
            try{
                respCode = conn.getResponseCode();
            }
            catch(ConnectionNotFoundException ex){
                //ex.printStackTrace();
                //midlet.switchDisplayable(null, midlet.mainMenu());
            }
            catch(IOException ex){
                //midlet.switchDisplayable(null, midlet.mainMenu());
            }
            
            System.out.println("RespCode = "+respCode);
            
            if(respCode == HttpConnection.HTTP_OK){//successful connection to server

                midlet.connectionIsAvailable = true;
                is= conn.openDataInputStream();
                
                /*int ch ;
                StringBuffer sb = new StringBuffer();
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb.toString());*/
                
                viewXML(is);
            }
            else{//server error
                midlet.connectionIsAvailable = false;
            }
        }
        catch(ParseException ex){
        }
        catch (ConnectionNotFoundException ex){// can't access server
            //ex.printStackTrace();
           //midlet.switchDisplayable(null, midlet.mainMenu());
           midlet.connectionIsAvailable = false;
        }
        catch (IOException ex) {
            //ex.printStackTrace();
            //midlet.switchDisplayable(null, midlet.mainMenu());
            midlet.connectionIsAvailable = false;
        }
        catch (Exception ex) {
            //ex.printStackTrace();
        }
        finally{
            if(is!=null){
                try {
                    is.close();
                } 
                catch (IOException ex) {
                }
            }
            if(os!=null){
                try {
                    os.close();
                } 
                catch (IOException ex) {
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } 
                catch (IOException ex) {
                }
            }
        }
        
        midlet.mainMenu().show();
    }
    
    private String decodeGuardRecords(){
        String udateSignature="";
        Vector id_Guard,lctn_officer,id_item;
        try{
            RecordStore rs = RecordStore.openRecordStore("guardRecords", true);
            if((rs.getNumRecords()>0)){//check wether recordStore in not empty
                RecordEnumeration rEnum = rs.enumerateRecords(null, null, false);// creates enumeration of records
                //byte [] bts = null;
                //while(rEnum.hasNextElement()){
                byte [] bts = rEnum.nextRecord();
                //break;
                //}
                String records = new String(bts,0,bts.length);//convert bytes into a string
                //records = 1389293309*{5241=MWESIGWA MOSES_777777777_Kampala, 1348=MUGABI EMMANUEL_777777777_Mbarara}
                Vector guardsRecords = AttendanceMidlet.split(records, "*");
                //
                udateSignature = guardsRecords.elementAt(0).toString();
                //udateSignature = 1389293309
                String guardset = guardsRecords.elementAt(1).toString();
                //guardset = {5241=MWESIGWA MOSES_777777777_Kampala, 1348=MUGABI EMMANUEL_777777777_Mbarara}
                String locationsSet = guardsRecords.elementAt(2).toString();
                System.out.println("locationsSet"+locationsSet);
                String itemsSet = guardsRecords.elementAt(3).toString();
                //System.out.println(locationsSet);
                Vector guards = AttendanceMidlet.split(guardset.substring(1, guardset.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //guards = [5241=MWESIGWA MOSES_777777777_Kampala,  1348=MUGABI EMMANUEL_777777777_Mbarara]
                for(int i=0;i<guards.size();i++){//create Hashtable of guards
                    id_Guard = AttendanceMidlet.split(guards.elementAt(i).toString(),"=");
                    // id_Guard = [5241,MWESIGWA MOSES_777777777_Kampala]
                    guardHashtable.put((id_Guard.elementAt(0).toString()).trim(), (id_Guard.elementAt(1).toString()).trim());
                    id_Guard.removeAllElements();
                }
                Vector locations = AttendanceMidlet.split(locationsSet.substring(1, locationsSet.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //
                for(int i=0;i<locations.size();i++){
                    lctn_officer = AttendanceMidlet.split(locations.elementAt(i).toString(),"=");
                    locationsHashTable.put(lctn_officer.elementAt(0).toString().trim(), lctn_officer.elementAt(1).toString().trim());
                    lctn_officer.removeAllElements();
                }
                Vector items = AttendanceMidlet.split(itemsSet.substring(1, itemsSet.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //
                for(int i=0;i<items.size();i++){
                    id_item = AttendanceMidlet.split(items.elementAt(i).toString(),"=");
                    String name_unit = id_item.elementAt(1).toString().trim();
                    itemsHashtable.put(id_item.elementAt(0).toString().trim(), name_unit);
                    unitsVector.addElement(AttendanceMidlet.split(name_unit,"_").elementAt(1).toString());
                    id_item.removeAllElements();
                }
                guards.removeAllElements();
                locations.removeAllElements();
                items.removeAllElements();
                
            }
            else{
                
                udateSignature="0";
            }
            rs.closeRecordStore();
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
        System.out.println("Prev Locations"+locationsHashTable);
        return udateSignature;
    }
    
    private void viewXML(InputStream ips){
        String locaction = new String();
        String action= new String();
        String id = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),
                deploymentOfficerName = new String(),deploymentOfficerID = new String(),
                deploymentOfficerMobileNumber= new String(),officerDets,
                actn = new String(),itemID = new String(),itemName = new String(),itemUnit = new String();
        try{
            InputStreamReader xmlReader = new InputStreamReader(ips);
            XmlParser parser = new XmlParser( xmlReader );
            ParseEvent pe;
            parser.skip();//skips <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, "reply");//start tag <updateResp>
            //
            boolean trucking = true;
            while (trucking) {
            pe = parser.read();
            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                pe = parser.read();
                currentDateTime= pe.getText();
                System.out.println(currentDateTime);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                pe = parser.read();
                upSignature = pe.getText();
                updateSign = upSignature;
                System.out.println(upSignature);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("upgradeURL")){
                pe = parser.read();
                upgradeURL = pe.getText();
                System.out.println(upgradeURL);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("connectionURL")){
                pe = parser.read();
                connectionURL = pe.getText();
                System.out.println(connectionURL);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                String nemu = pe.getName();
                System.out.println("(1) = "+pe.getType());
                System.out.println("(1) = "+nemu);
                if (nemu.equals("update")) {
                    while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                        parser.skip();
                        pe = parser.peek();
                        System.out.println("(2) = "+pe.getType());
                        System.out.println("(2) = "+pe.getName());
                        if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                            String name1 = pe.getName();
                            if (name1.equals("record")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                        pe = parser.read();
                                        gName = pe.getText();
                                        //name = gName;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                        pe = parser.read();
                                        number = pe.getText();
                                        //phoneNum = number;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                        pe = parser.read();
                                        id = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                       pe = parser.read();
                                        action = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                        pe = parser.read();
                                        locaction = pe.getText();
                                    }
                                }
                                //hashtable for guards
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(number==null||locaction==null){
                                        number = "null";
                                        locaction = "null";
                                    }
                                    guardHashtable.put(id.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    guardHashtable.remove(id.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                    pe = parser.read();
                            }
                        }
                        else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("site")){
                            System.out.println("(3) = "+pe.getType());
                            System.out.println("(3) = "+pe.getName());
                            String nam = pe.getName();
                            if (nam.equals("site")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteName")) {
                                        pe = parser.read();
                                        deploymentName = pe.getText();
                                        System.out.println(deploymentName);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                        //System.out.println(action);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteID")) {
                                        pe = parser.read();
                                        deploymentID = pe.getText();
                                    }
                                }
                                //hashtable for locations
                                officerDets = /*deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+*/deploymentName.trim();
                                if("INSERT".equalsIgnoreCase(action)){
                                    locationsHashTable.put(deploymentID.trim(), officerDets);
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    locationsHashTable.remove(deploymentID.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                    pe = parser.read();
                            }
                        }
                        else if (pe.getType() == Xml.START_TAG&&pe.getName().equals("item")){
                            String atbNemu = pe.getName();
                            if (atbNemu.equals("item")) {
                                while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(atbNemu) == false)){
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemID")) {
                                        pe = parser.read();
                                        itemID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemName")) {
                                        pe = parser.read();
                                        itemName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemUnit")) {
                                        pe = parser.read();
                                        itemUnit = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                    }
                                }
                                //hashtable for items
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(itemID==null||itemName==null||itemUnit==null){
                                        itemUnit = "null";
                                        itemName = "null";
                                        itemID = "null";
                                    }
                                    unitsVector.addElement(itemUnit);//avoid duplicates
                                    itemsHashtable.put(itemID.trim(), itemName.trim()+"_"+itemUnit.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    itemsHashtable.remove(itemID.trim());
                                }
                            }
                            else{
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(atbNemu) == false))
                                    pe = parser.read();
                            }
                        }
                    }
                }
                else {
                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                    pe = parser.read();
                }
            }
            if (pe.getType() == Xml.END_TAG &&pe.getName().equals("reply"))
                trucking = false;
            }
            System.out.println("Guards Hashtable: "+guardHashtable.toString());
            System.out.println("Locations Hashtable: "+locationsHashTable.toString());
            System.out.println("Items Hashtable: "+itemsHashtable.toString());
            today = AttendanceMidlet.split(currentDateTime, " ").elementAt(0).toString();
            RecordStore.deleteRecordStore("guardRecords");//remove previous records
            byte [] byts = (/*upSignature*/"0"+"*"+guardHashtable.toString()+"*"+locationsHashTable.toString()+"*"+itemsHashtable.toString()).getBytes();//get byte representation
            RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
            rst.addRecord(byts, 0, byts.length);
            rst.closeRecordStore();//save update guard records
            midlet.id = pin;
        }
        catch (Exception ex) { 
            ex.printStackTrace();
        }
    }
}
